**Recipe Site Script Free**

Setup and manage your own recipe site with this free php site script.

**About Recipe Site Script Free**

The recipe site script can be easily installed to create a food recipe directory. 

This is a community based release that is supplied without support or liability. You are free to use the fonts site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Features:**

* Written in php/mysql.
* All pages are created using Apaches mod-rewrite so any recipe page all look like static pages within the search engines.
* Comes with over 22,000 recipes.
* 100% source code.
* Recipes managed into over 150 different categories.
* Search feature which searches recipes titles, ingredients and categories.
* Basic admin area to add new recipes.
* 'Friends' page for ease of management of links exchanges.
* Recent search feature which lists the 30 most recent search items and links into the site.
* Use html code within added recipes.
* Original logo fireworks png file included. (inc font)
* Ideal for adding adsense or other code.
* Users can comment on recipes.
* Users can rate recipes.
* Ratings/Comments are protected with captcha images.

**Requirements**

* php 4.x/5.x
* mysql
* mod rewrite module on apache
* GD library within php for security images on comments
* HD/BW requirements will vary on your traffic, scripts require about 100kb.

**Installation**

Full instructions are given in the ___SETUP_INSTRUCTIONS.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/recipe-site-script-free

**License**

Recipe Site Script Free is copyrighted by http://mfscripts.com and is released under the http://opensource.org/licenses/MIT. You are free to use the recipe site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/recipe-site-script-free